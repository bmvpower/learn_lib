import {getBooks, getTableHead, removeBooks, addRemoveBooks, deleteRemoveBooks} from './Хранилище/bookStore.js'
import {showDescription} from "./Шаблоны/description.js";

function createTable() {
  let table = document.createElement ( 'table');
  return table;
}

function createTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement( 'th');
    th.classList.add('cth');
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);

  }
}

function rowOnclick(id) {
  showDescription(id)
}

function createTableBody(table, data) {
  let tbody = table.createTBody();
  data.forEach((book) => {
    let row = tbody.insertRow();

    for (let key in book) {
      if (key === 'image') {
        continue;
      }
        let cell = row.insertCell();
      cell.classList.add('cth');
      let text = document.createTextNode(book[key]);
      cell.append(text);
      cell.onclick = () => rowOnclick(book.id);
    }
    let checkCell = row.insertCell();
    checkCell.classList.add('cth');
    checkCell.appendChild(createremoveBtn(book));
  });
  return tbody;
}

function createremoveBtn(book) {
  let removeBtn = document.createElement('button');
  removeBtn.textContent = 'Delete';
  removeBtn.onclick = () => {
    let table = document.querySelector('table');
    addRemoveBooks(book.id);
    removeBooks();
    document.querySelector('tbody').replaceWith(createTableBody(table, getBooks()));
  };
  return removeBtn;
}

function main1() {
  let mainDiv = document.getElementById( 'main1');
  let table = createTable();
  mainDiv.appendChild(table);
  createTableHead(table, getTableHead());
  createTableBody(table, getBooks());
}
main1();
let books = [
  {id: 1, author: 'Пушкин А.С.', title: 'Руслан и Людмила', year: 1820, description: 'Поэма'},
  {id: 2, author: 'Лермонтов М.Ю.', title: 'Бородино', year: 1837, description: 'Стихотворение'},
  {id: 3, author: 'Чехов', title: 'Палата №6', year: 1892, description: 'Повесть'},
  {id: 4, author: 'Достоевский', title: 'Идиот', year: 1869, description: 'Роман'},
] ;

let tableHead = ['id', 'Автор', 'Название', 'Год', 'Описание'] ;

let removeBooksIds = [];

export function getBooks() {
  return books;
}

export function getTableHead() {
  return tableHead;
}

export function getBookById(id) {
  return books.filter(item => item.id === id)[0];
}

export function removeBooks (bookIds) {
  books = books.filter(book => !removeBooksIds.includes(book.id));
}

export function addRemoveBooks(id) {
  removeBooksIds.push(id);
}

export function deleteRemoveBooks(removeId) {
  removeBooksIds = removeBooksIds.filter(id => id !== removeId);
}
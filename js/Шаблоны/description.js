import {getBookById} from "../Хранилище/bookStore.js";

export function  showDescription(id) {
  let book = getBookById(id);
  console.log(book);

  let descBook = document.createElement('div');
  descBook.classList.add('desc');
  let table = document.querySelector('table');
  table.replaceWith(descBook);

  let wrapD = document.createElement('wrap');
  wrapD.classList.add('wrap');
  descBook.appendChild(wrapD);

  let div1 = document.createElement('div');
  div1.classList.add('div1');

  let div2 = document.createElement('div');
  div1.classList.add('div2');

  wrapD.appendChild(div1);
  wrapD.appendChild(div2);

  createProp(book, div2);

  let image = document.createElement('img');
  image.classList.add('img');
  div1.appendChild(image);

  image.src='../../assets/Пушк.jpg';
  image.alt='Автор';

  let backButton = document.createElement('button');
  backButton.textContent = 'Назад';
  backButton.onclick = ()=> {
    descBook.replaceWith(table);
  };

  descBook.appendChild(backButton);

  backButton.style.MozBorderRadius = '25px';
  backButton.style.WebkitBorderRadius = '25px';
}

function createProp(book, descBook) {
  for (let key in book) {

    let inputElement = document.createElement('input');
    inputElement.value = book[key];

    //input.textContent = book[key];
    descBook.append(inputElement);

    if (key === 'image') {
      inputElement.classList.add('img')
    }

  }
}
